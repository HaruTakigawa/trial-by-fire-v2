﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClampPosition : MonoBehaviour
{
    public Vector3 minPos;
    public Vector3 maxPos;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, minPos.x, maxPos.x),
        Mathf.Clamp(transform.position.y, minPos.y, maxPos.y),
        Mathf.Clamp(transform.position.z, minPos.z, maxPos.z));
    }
}
