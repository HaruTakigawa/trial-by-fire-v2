﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireAudioTrigger : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D col)
    {
        if (!FindObjectOfType<AudioManager>().isPlaying("Fire") && col.gameObject.name == "Normal")
        {
            FindObjectOfType<AudioManager>().Play("Fire");
        }
    }
}
