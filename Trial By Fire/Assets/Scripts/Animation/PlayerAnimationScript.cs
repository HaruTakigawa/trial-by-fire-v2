﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationScript : MonoBehaviour
{
    Animator anim;
    PlayerController controller;
    Axe axe;
    Extinguisher ext;
    // Start is called before the first frame update
    void Start()
    {
        controller = this.GetComponent<PlayerController>();
        anim = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetBool("IsWalking", controller.isMoving);

        if (controller.isCrouching == true)
        {
            anim.SetBool("IsCrouching", true);
        }
        //set if player anim is vaulting
        if (controller.isVaulting == true)
        {
            anim.SetBool("IsVaulting", true);
        }

        if(controller.isVaulting == false)
        {
            anim.SetBool("IsVaulting", false);
        }

        if (controller.isMoving == false && controller.isCrouching == true)
        {
            anim.SetBool("IsCrouchWalking", false);
        }
        // Set the player animation to crouch while running
        if (controller.isMoving == true && controller.isCrouching == true)
        {
            anim.SetBool("IsCrouchWalking", true);
        }

        // Sets the player back to idle animation from crouching
        if (controller.isCrouching == false)
        {
            anim.SetBool("IsCrouching", false);
            anim.SetBool("IsCrouchWalking", false);
        }

        // Switches from Extinguisher to Axe
        if (Input.GetKey(KeyCode.Alpha1) && controller.inventory.EquippedItem.ItemName == "Axe")
        {
            anim.SetBool("IsPickingUpAxe", true);
            anim.SetBool("IsPickingUp", false);
        }
        // Switches from Axe to Extinguisher
        if (Input.GetKey(KeyCode.Alpha2) && controller.inventory.EquippedItem.ItemName == "Extinguisher")
        {
            anim.SetBool("IsPickingUp", true);
            anim.SetBool("IsPickingUpAxe", false);
        }
        // Weapon switching animation while running
        if (controller.isMoving == true && controller.inventory.EquippedItem.ItemName == "Axe")
        {
            anim.SetBool("IsRunSwitching", false);
            anim.SetBool("IsPickingUpAxe", true); // Allows the player to change to running axe animation when picked up while running
            anim.SetBool("IsPickingUp", false);
        }

        if (controller.isMoving == true && controller.inventory.EquippedItem.ItemName == "Extinguisher")
        {
            anim.SetBool("IsRunSwitching", true);
            anim.SetBool("IsPickingUp", true); // Allows the player to change to running ext animation when picked up while running
            anim.SetBool("IsPickingUpAxe", false);
        }

        if (controller.inventory.EquippedItem.ItemName != "Axe" & controller.inventory.EquippedItem.ItemName == "Extinguisher")
        {
            if (Input.GetKey(KeyCode.F))
            {
                anim.SetBool("IsPickingUp", true);
                anim.SetBool("IsPickingUpAxe", false);
            }
        }

        if (controller.inventory.EquippedItem.ItemName != "Extinguisher" & controller.inventory.EquippedItem.ItemName == "Axe")
        {
            if (Input.GetKey(KeyCode.F))
            {
                anim.SetBool("IsPickingUp", false);
                anim.SetBool("IsPickingUpAxe", true);
            }
        }   

        // Reverts to idle animation if Axe isn't in inventory
        if (Input.GetKey(KeyCode.Alpha1) & controller.inventory.EquippedItem.ItemName != "Axe")
        {
            anim.SetBool("IsPickingUp", false);
            anim.SetBool("IsPickingUpAxe", false);
        }
        // Reverts to idle animation if Extinguisher isn't in inventory
        if (Input.GetKey(KeyCode.Alpha2) & controller.inventory.EquippedItem.ItemName != "Extinguisher")
        {
            anim.SetBool("IsPickingUp", false);
            anim.SetBool("IsPickingUpAxe", false);
        }
        // Reverts to idle animation if no item is equipped
        if (controller.inventory.EquippedItem.ItemName != "Axe" & controller.inventory.EquippedItem.ItemName != "Extinguisher")
        {
            anim.SetBool("IsPickingUp", false);
            anim.SetBool("IsExtinguishing", false);
            anim.SetBool("IsPickingUpAxe", false);
            //Debug.Log("No Item Equipped Animation");
        }

        if (controller.isHurt == true)
        {
            anim.SetBool("IsOnFire", true);
        }

        if (controller.isHurt == false)
        {
            anim.SetBool("IsOnFire", false);
        }

        if (controller.inventory.EquippedItem != null)
        {
            if (controller.inventory.EquippedItem.ItemName == "Extinguisher")
            {
                anim.SetBool("IsExtinguishing", controller.inventory.EquippedItem.ItemName == "Extinguisher" && controller.inventory.EquippedItem.isUsingItem);
                anim.SetBool("IsPickingUp", controller.inventory.EquippedItem.ItemName == "Extinguisher");

                if (controller.inventory.EquippedItem.ItemName == "Extinguisher" && controller.inventory.EquippedItem.GetComponent<Extinguisher>().usable)
                {
                    anim.SetBool("IsPickingUp", true);
                    anim.SetBool("IsExtinguishing", false);
                }

            }
            if (controller.inventory.EquippedItem.ItemName == "Axe")
            {
                anim.speed = 1f;
                if (Input.GetMouseButtonDown(0))
                {
                    anim.SetBool("IsChopping", true);
                }

                else if (Input.GetMouseButtonUp(0))
                {
                    anim.SetBool("IsChopping", false);
                }

                anim.SetBool("IsPickingUpAxe", controller.inventory.EquippedItem.ItemName == "Axe");
            }
        }
        /*
        if(this.GetComponent<PlayerController2>().currentItem != null)
        {
            this.GetComponent<Animator>().SetBool("IsChopping", this.GetComponent<PlayerController2>().currentItem.UniqueID == 1 && this.GetComponent<PlayerController2>().IsUsingWeapon);
            this.GetComponent<Animator>().SetBool("IsPickingUp", this.GetComponent<PlayerController2>().currentItem.UniqueID == 1);
        }*/

    }
}
