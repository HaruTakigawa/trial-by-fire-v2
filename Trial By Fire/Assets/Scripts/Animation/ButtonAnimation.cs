﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonAnimation : MonoBehaviour
{
    public Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void ButtonHoverAnim(bool animating)
    {
        anim.SetBool("IsHovering", animating);
    }
}
