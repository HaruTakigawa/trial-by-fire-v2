﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebrisAnimation : MonoBehaviour
{
    public Barricade barricade;
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        anim.SetBool("IsAtForm2", false);
        anim.SetBool("IsAtForm3", false);
        anim.SetBool("IsAtForm4", false);
    }

    // Update is called once per frame
    void Update()
    {
        SwitchFormAnim();
    }

    void SwitchFormAnim()
    {
        if (barricade.percentageHealth >= 1)
        {
            anim.SetBool("IsAtForm1", true);
            //Debug.Log("1st form");
        }

        if (barricade.percentageHealth <= 0.75 && barricade.percentageHealth >= 0.50)
        {
            anim.SetBool("IsAtForm2", true);
            anim.SetBool("IsAtForm1", false);
            //Debug.Log("2nd form");
        }

        if (barricade.percentageHealth <=  0.50 && barricade.percentageHealth >= 0)
        {
            anim.SetBool("IsAtForm3", true);
            anim.SetBool("IsAtForm2", false);
            //Debug.Log("3rd form");
        }

        if (barricade.percentageHealth == 0)
        {
            anim.SetBool("IsAtForm4", true);
            anim.SetBool("IsAtForm3", false);
            //Debug.Log("4th form");
        }
    }
}
