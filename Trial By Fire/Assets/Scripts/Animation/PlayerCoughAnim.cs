﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCoughAnim : MonoBehaviour
{
    Animator anim;
    Smoke smoke;
    
    // Start is called before the first frame update
    void Start()
    {
        smoke = this.GetComponentInChildren<Smoke>();
        anim = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (smoke.curSmoke >= smoke.maxSmoke)
        {
            anim.SetBool("IsCoughing", true);
        }

        else
        {
            anim.SetBool("IsCoughing", false);
        }
    }
}
