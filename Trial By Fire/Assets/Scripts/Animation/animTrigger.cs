﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animTrigger : MonoBehaviour
{

    Animator anim;
    Obstacles obstacle;
    public GameObject highlight;

    //public bool playAnim;

    void Start()
    {
        obstacle = GetComponent<Obstacles>();
        anim = GetComponent<Animator>();
        anim.speed = 0;
    }

    private void Update()
    {
        //if (playAnim == true)
        //{
        //    playAnimation();
        //}
        if (obstacle.curHealth <= 0)
        {
            playAnimation();
            Destroy(highlight);
        }
    }

    public void playAnimation()
    {
        anim.speed = 1;
    }

}
