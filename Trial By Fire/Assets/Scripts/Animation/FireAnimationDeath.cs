﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireAnimationDeath : MonoBehaviour
{
    public Fires fires;
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        SwitchFormAnim();
    }   

    void SwitchFormAnim()
    {
        if (fires.curHealth == fires.maxHealth)
        {
            anim.SetBool("IsAtForm1", true);
            anim.SetBool("IsAtDyingForm", false);
           // Debug.Log("Fire burning bright");
        }

        if (fires.curHealth > fires.maxHealth * 0.50)
        {
            anim.SetBool("IsAtForm1", true);
            anim.SetBool("IsAtDyingForm", false);
        }

        if (fires.curHealth < fires.maxHealth * 0.50)
        {
           anim.SetBool("IsAtDyingForm", true);
           anim.SetBool("IsAtForm1", false); 
          // Debug.Log("Fire is dying");
        }
    }
}
