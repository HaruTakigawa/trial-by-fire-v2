﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LungAnimation : MonoBehaviour
{
    public Smoke lungs;
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (lungs.curSmoke >= lungs.maxSmoke)
        {
            anim.SetBool("IsExpandingLungs", true);
        }

        else
        {
            anim.SetBool("IsExpandingLungs", false);
        }
    }
}
