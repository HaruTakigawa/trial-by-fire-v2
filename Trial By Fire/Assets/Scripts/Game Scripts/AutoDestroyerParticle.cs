﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroyerParticle : MonoBehaviour
{
    ParticleSystem dmgParticle;
    // Start is called before the first frame update
    void Start()
    {
        dmgParticle = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if(dmgParticle.IsAlive() != true)
        {
            Destroy(gameObject);
        }
    }
}
