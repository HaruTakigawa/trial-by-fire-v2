﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vault : MonoBehaviour
{
    public GameObject vaultTo;
    public bool isLeftSide;
    void Update()
    {
        if (this.gameObject.GetComponentInParent<Barricade>().curHealth <= 0)
        {
            vaultTo.GetComponent<BoxCollider2D>().enabled = true;
            this.gameObject.GetComponent<BoxCollider2D>().enabled = true;
        }
        else
        {
            vaultTo.GetComponent<BoxCollider2D>().enabled = false;
            this.GetComponent<BoxCollider2D>().enabled = false;
        }
    }
}
