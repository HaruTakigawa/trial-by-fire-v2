﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    public GameObject Player;
    public Transform teleportLocation;
    public bool CantUse;

    void OnTriggerEnter2D(Collider2D col)
    {
        print("Collided with player");
        if (Input.GetKeyDown(KeyCode.E))
        {
            TeleportPlayer();
        }
    }

    public void TeleportPlayer()
    {
        Player.transform.position = teleportLocation.transform.position;
    }
}
