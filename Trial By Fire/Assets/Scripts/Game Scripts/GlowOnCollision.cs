﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlowOnCollision : MonoBehaviour
{
    public GameObject ItemGlow;
    Barricade barricade;
    public bool ctrlIcon;
    public bool isTimed;
    public float timerValue;
    private float timer;
    private bool isPlayer;
    void Start()
    {
        timer = timerValue;
        ItemGlow.SetActive(false);
        barricade = this.gameObject.GetComponentInParent<Barricade>();
    }
    void Update()
    {
        if (isPlayer)
        {
            if (isTimed)
            {
                timerValue -= Time.deltaTime;
                if (timerValue <= 0)
                {
                    timerValue = 0;
                    ItemGlow.SetActive(false);
                }
                else
                {
                    isBarricade();
                }

            }
            else
            {
                isBarricade();
            }
        }
        else
        {
            if (isTimed)
            {
                timerValue = timer;
            }
        }
    }
    void isBarricade()
    {
        if (this.gameObject.GetComponentInParent<Barricade>())
        {
            if (ctrlIcon)
            {
                if (barricade.curHealth <= 0)
                {
                    ItemGlow.SetActive(true);
                }
                else
                {
                    ItemGlow.SetActive(false);
                }
            }
            else
            {
                if (barricade.curHealth <= 0)
                {
                    ItemGlow.SetActive(false);
                }
                else
                {
                    ItemGlow.SetActive(true);
                }
            }
        }
        else
        {
            ItemGlow.SetActive(true);
        }
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.GetComponent<PlayerController>())
        {
            isPlayer = true;
        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.GetComponent<PlayerController>())
        {
            ItemGlow.SetActive(false);
            isPlayer = false;
        }
    }
}
