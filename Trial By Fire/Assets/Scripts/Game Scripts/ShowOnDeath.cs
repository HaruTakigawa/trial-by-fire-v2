﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowOnDeath : MonoBehaviour
{
    public GameObject[] ObjectsToShow;
    Obstacles obstacle;

    int i;

    void Start()
    {
        i = 0;
        while(i < ObjectsToShow.Length)
        {
            ObjectsToShow[i].SetActive(false);
            i++;
        }

        obstacle = GetComponentInParent<Obstacles>();
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (this.gameObject.GetComponentInParent<Barricade>().curHealth <= 0)
            {
                ShowObjects(true);
            }
        }
    }

    void ShowObjects(bool show)
    {
        i = 0;
        while (i < ObjectsToShow.Length)
        {
            ObjectsToShow[i].SetActive(show);
            i++;
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        ShowObjects(false);
    }
}
