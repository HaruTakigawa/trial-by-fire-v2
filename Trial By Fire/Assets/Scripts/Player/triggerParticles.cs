﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triggerParticles : MonoBehaviour
{
    ParticleSystem particles;
    public bool allowParticle = false;

    private void Start()
    {
        particles = GetComponent<ParticleSystem>();
    }

    private void Update()
    {
        if(allowParticle)
        {
            Invoke("PlayParticle", 0f);
        }
        else
        {
            StopParticle();
            CancelInvoke();
        }
    }


    private void PlayParticle()
    {
        particles.Play();
    }
    private void StopParticle()
    {
        particles.Stop();
    }

}
