﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Collections;
using UnityEngine.Audio;

public class PlayerAudio : MonoBehaviour
{
    public AudioManager sound;
    PlayerController player;
    
    int index;

    // Start is called before the first frame update
    void Start()
    {
        sound = FindObjectOfType<AudioManager>();
        player = this.GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Footstep()
    {
        Debug.Log("Footstep" + index + " played");

        index = UnityEngine.Random.Range(1, 3);

        if(index == 1)
        {
            sound.Play("Footstep1");

        }
        else if (index == 2)
        {
            sound.Play("Footstep2");
        }
        else if (index == 3)
        {
            sound.Play("Footstep3");
        }
    }

    public void Axe()
    {
        Debug.Log("Axe audio played");
        sound.Play("Axe");
    }

    public void ExtinguisherStart()
    {
        if(!sound.isPlaying("Extinguisher"))
        {
            sound.Play("Extinguisher");
        }
    }

    public void ExtinguisherStop()
    {
        sound.Stop("Extinguisher");
    }
}
