﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public Inventory inventory;
    public bool isUsingItem;
    public string ItemName;
    public LayerMask Layer;
    public float AttackRange;
    public float Damage;
    public bool CanAttack = true ;
    public Vector3 DroppedItem;
    public Quaternion DroppedItemRotation;
    
    
    public virtual void UseItem()
    {
        if (inventory.EquippedItem.name == "Extinguisher")
        {
            print("Extinguisher Used");
            // Do This
        }
        else if (inventory.EquippedItem.name  == "Axe")
        {
            // Do this
            print("Axe Used");
        }
    }
    public void DropItem()
    {
        if(ItemName == "EmptyItem") return;
        this.GetComponent<Collider2D>().enabled = true;
        this.GetComponent<Rigidbody2D>().simulated = true;
        this.GetComponent<SpriteRenderer>().enabled = true;
        this.gameObject.transform.parent = null;
        this.gameObject.transform.position += DroppedItem;
        this.gameObject.transform.rotation = DroppedItemRotation;
    }
        public void CarryItem(GameObject Player)
    {
        this.GetComponent<Collider2D>().enabled = false;
        this.GetComponent<Rigidbody2D>().simulated = false;
        this.GetComponent<SpriteRenderer>().enabled = false;
        this.gameObject.transform.parent = Player.gameObject.transform;
        this.gameObject.transform.position = Player.gameObject.transform.position;
    }
}
