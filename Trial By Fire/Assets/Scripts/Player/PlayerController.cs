﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    BathroomWin bathroom;
    public Inventory inventory;
    Rigidbody2D rb;
    public bool isMoving;
    public bool isJumping;
    public bool isCrouching;
    public bool isHurt;
    public float hurtTimer;
    public Vector2 repelValue;
    // Old code
    public bool m_FacingRight = true;
    public float speed;
    public float movementDelay;
    float horizontalMove = 0f;
    Vector2 moveVelocity;

    public float jumpHeight = 4;
    public float timeToJumpApex = .4f;
    private float jumpVelocity;
    private float gravity;

    // Test
    public LayerMask whatIsGround;
    public LayerMask whatIsCeiling;
    public float groundRadius;
    public float ceilingRadius;
    public Transform groundPoint;
    public Transform ceilingPoint;
    public bool ceiling;
    public bool grounded;

    private float crouch;
    private float originalSpeed;
    private float crouchSpeed;

    // 
    private Collider2D[] Collided;
    void Start()
    {
        bathroom = GetComponent<BathroomWin>();
        crouchSpeed = speed * 0.5f;
        originalSpeed = speed;
        rb = this.GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        gravity = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
        crouch = Input.GetAxisRaw("Crouch");
        Crouch();
        //movement and level interaction
        if (!isHurt && !isVaulting && grounded)
        {
            Move();
            isJumping = false;
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            InteractItems();
        }
        else if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S))
        {
            Interact();
        }
        if (Input.GetKeyDown(KeyCode.Space) && !isVaulting && grounded && !isHurt && !isCrouching && !this.gameObject.GetComponentInChildren<Smoke>().isCoughing)
        {
            isJumping = true;
            rb.velocity = Vector2.up * jumpVelocity;
            rb.velocity = new Vector2(rb.velocity.x, jumpVelocity);
        }
        if (isVaulting) return;
        if (Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.C) && !isVaulting)
        {
            isCrouching = true;
        }
        if (isCrouching)
        {
            speed = crouchSpeed;
        }
        else if (!isCrouching)
        {
            speed = originalSpeed;
        }
    }
    void FixedUpdate()
    {
        if (!isHurt)
        {
            rb.velocity = new Vector2(horizontalMove, rb.velocity.y);
        }
        grounded = Physics2D.OverlapCircle(groundPoint.position, groundRadius, whatIsGround);
        ceiling = Physics2D.OverlapCircle(ceilingPoint.position, ceilingRadius, whatIsCeiling);
        // rb.MovePosition(rb.position + moveVelocity * Time.fixedDeltaTime);
    }
    void Crouch()
    {
        if (isVaulting) return;
        if ((crouch != 0 || ceiling) && grounded)
        {
            isCrouching = true;
        }
        else
        {
            isCrouching = false;
        }
    }
    void Move()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal") * speed;
        //verticalMove = Input.GetAxisRaw("Vertical") * speed;

        Vector2 moveInput = new Vector2(horizontalMove, Input.GetAxisRaw("Vertical"));
        moveVelocity = moveInput.normalized * speed;
        // Temporary code for animation trigger
        if (Input.GetKey(KeyCode.A))
        {
            isMoving = true;
        }

        if (Input.GetKey(KeyCode.D))
        {
            isMoving = true;
        }

        //Stopping walk animation

        if (Input.GetKeyUp(KeyCode.A))
        {
            isMoving = false;
        }

        if (Input.GetKeyUp(KeyCode.D))
        {
            isMoving = false;
        }


        if (horizontalMove > 0 && !m_FacingRight)
        {
            Flip();
        }
        else if (horizontalMove < 0 && m_FacingRight)
        {
            Flip();
        }
        // float xSpeed = Input.GetAxis("Horizontal") * PlayerSpeed;
        // float ySpeed = Input.GetAxis("Vertical") * PlayerSpeed;
        // rb.velocity = new Vector2(xSpeed, ySpeed);
        // if (xSpeed != 0 || ySpeed != 0) isMoving = true;
        // else isMoving = false;
    }
    //raycasting front to check if theres a slope
    //if theres a slope travel there first
    //while pressing down and forward or back ignore slope.
    private void Flip()
    {
        m_FacingRight = !m_FacingRight;

        transform.Rotate(0f, 180f, 0f);
    }
    public void Interact()
    {
        Collided = new Collider2D[1];
        this.GetComponent<Collider2D>().OverlapCollider(new ContactFilter2D()
        {
            layerMask = LayerMask.GetMask("Interactable"),
            useTriggers = true,
            useLayerMask = true
        }, Collided);

        if (Collided[0] == null)
            return;
        else if (Collided[0].GetComponent<Teleporter>())
        {
            if (Collided[0].GetComponent<Teleporter>().goingUp && Input.GetKeyDown(KeyCode.W))
            {
                Collided[0].GetComponent<Teleporter>().Travel(this.gameObject);
            }
            else if (!Collided[0].GetComponent<Teleporter>().goingUp && Input.GetKeyDown(KeyCode.S))
            {
                Collided[0].GetComponent<Teleporter>().Travel(this.gameObject);
            }
            else
            {
                print("Failed");
            }
        }
        else
        {
            print("Interacted");
        }
    }
    public void InteractItems()
    {

        Collided = new Collider2D[1];

        this.GetComponent<Collider2D>().OverlapCollider(new ContactFilter2D()
        {
            layerMask = LayerMask.GetMask("Interactable"),
            useTriggers = true,
            useLayerMask = true
        }, Collided);

        if (Collided[0] == null)
            return;

        else if (Collided[0].gameObject.tag == "Sink")
        {
            print("Towel Wet");
            for (int i = 0; i < this.GetComponent<Inventory>().UsableList.Count; i++)
            {
                this.GetComponent<Inventory>().UsableList[i].GetComponent<Towel>().isWet = true;
            }
        }
        else if (Collided[0].GetComponent<Vault>())
        {
            if (isVaulting) return;
            isMoving = false;
            StartCoroutine("Vaulting");
        }
        else if (Collided[0].gameObject.tag == "Bathroom")
        {
            if (this.GetComponent<Inventory>().UsableList.Count < 4)
            {
                return;
                // inventory.UsableList.Clear();
                // Collided[0].GetComponent<Bathroom>().bathroomDoorLock = true;
            }
            if (this.GetComponent<Inventory>().UsableList.Count >= 4)
            {
                // for (int i = 0; i < 2; i++)
                // {
                //     inventory.UsableList.Remove(inventory.UsableList[i]);
                // }
                inventory.UsableList.Clear();
                Collided[0].GetComponent<Bathroom>().bathroomDoorLock = true;
            }
            else
            {
                print("Not Enough Towels");
            }
        }
        else
        {
            print("Interacted");
        }
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.GetComponent<Fires>())
        {
            StartCoroutine("Hurt");
            isMoving = false;
            // Setup inenumerator for hurt state
        }
    }
    IEnumerator Hurt()
    {
        print("Hurt");
        isHurt = true;
        rb.velocity = Vector2.zero;
        if (m_FacingRight)
            rb.AddForce(new Vector2(-repelValue.x, repelValue.y));
        if (!m_FacingRight)
            rb.AddForce(new Vector2(repelValue.x, repelValue.y));
        yield return new WaitForSeconds(hurtTimer);
        isHurt = false;
    }
    public bool isVaulting;
    IEnumerator Vaulting()
    {
        print("Vaulting");
        rb.velocity = Vector2.zero;
        horizontalMove = 0;
        isVaulting = true; 
        if (Collided[0].GetComponent<Vault>().isLeftSide && m_FacingRight)
        {
            yield return new WaitForSeconds(movementDelay);
            this.gameObject.transform.position = Collided[0].GetComponent<Vault>().vaultTo.transform.position;
        }
        else if (!Collided[0].GetComponent<Vault>().isLeftSide && !m_FacingRight)
        {
            yield return new WaitForSeconds(movementDelay);
            this.gameObject.transform.position = Collided[0].GetComponent<Vault>().vaultTo.transform.position;
        }
        else
        {
            print("Try Again");
        }
        isVaulting = false;
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(ceilingPoint.position, ceilingRadius);
        Gizmos.DrawWireSphere(groundPoint.position, groundRadius);
    }
}
