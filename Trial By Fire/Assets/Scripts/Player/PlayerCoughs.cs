﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCoughs : MonoBehaviour
{
    public int MinTime;
    public int MaxTime;
    private float PlayerMovesForSeconds;
    public float PlayerCoughsForSeconds;

    public PlayerController movementRef;

    IEnumerator PlayerCough()
    {
        while (true)
        {
            yield return new WaitForSeconds(PlayerMovesForSeconds);

            movementRef.enabled = false;

            yield return new WaitForSeconds(PlayerCoughsForSeconds);

            PlayerMovesForSeconds = Random.Range(MinTime, MaxTime);
            Debug.Log(PlayerMovesForSeconds);
            movementRef.enabled = true;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(PlayerCough());
        PlayerMovesForSeconds = Random.Range(MinTime, MaxTime);
        Debug.Log(PlayerMovesForSeconds);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
