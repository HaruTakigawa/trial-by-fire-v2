﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<Item> UsableList;
    public List<Item> EquipmentList;
    public Item EquippedItem;
    public Item EmptyItem;
    public Collider2D[] CollidedItems;
    public Transform AttackTransform;
    public Transform AxeAttackTransform;
    public triggerParticles trigger;

    // Update is called once per frame
    PlayerController Player;
    PlayerAudio pSound;
    Smoke PlayerSmoke;
    void Start()
    {
        Player = this.gameObject.GetComponent<PlayerController>();
        PlayerSmoke = this.gameObject.GetComponentInChildren<Smoke>();
        pSound = this.gameObject.GetComponent<PlayerAudio>();
    }

    void Update()
    {
        if (Player.isCrouching || Player.isHurt || PlayerSmoke.isCoughing || Player.isJumping)
        {
            trigger.allowParticle = false;
            return;
        }

        Vector2 dir = (AttackTransform.position - this.gameObject.transform.position);
        RaycastHit2D[] hits = Physics2D.RaycastAll(this.gameObject.transform.position, dir, 8f);

        //Change equipped item
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (EquipmentList[0] == null) return;
            if (EquippedItem.isUsingItem) return;
            EquippedItem = EquipmentList[0];
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            if (EquipmentList[1] == null) return;
            if (EquippedItem.isUsingItem) return;
            EquippedItem = EquipmentList[1];
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            if (EquipmentList[2] == null) return;
            if (EquippedItem.isUsingItem) return;
            EquippedItem = EquipmentList[2];
        }
        else if (Input.GetKey(KeyCode.Mouse0))
        {

            if (EquippedItem == null) return;
            if (EquippedItem.ItemName == "EmptyItem") return;


            if (EquippedItem.ItemName != "Extinguisher")
            {
                trigger.allowParticle = false;
            }
            if (EquippedItem.ItemName == "Extinguisher")
            {
                if (!EquippedItem.GetComponent<Extinguisher>().usable)
                {
                    trigger.allowParticle = false;
                    EquippedItem.DropItem();
                    EquipmentList[EquipmentList.FindIndex(ind => ind.Equals(EquippedItem))] = EmptyItem;
                    EquippedItem = EmptyItem;
                    return;
                }
                trigger.allowParticle = true;
                pSound.ExtinguisherStart();
                if (hits.Length <= 0) EquippedItem.GetComponent<Extinguisher>().curUses += Time.deltaTime * 1;
                for (int i = 0; i < hits.Length; i++)
                {
                    RaycastHit2D hit = hits[i];
                    if (hit.collider.GetComponent<Barricade>())
                    {
                        EquippedItem.GetComponent<Extinguisher>().curUses += Time.deltaTime * 1;
                        return;
                    }
                    if (hit.collider.tag == "Fire")
                    {
                        EquippedItem.UseItem();
                    }
                    else
                        EquippedItem.GetComponent<Extinguisher>().curUses += Time.deltaTime * 1;
                }
            }

            if (EquippedItem.ItemName == "Axe")
            {
                EquippedItem.UseItem();
            }
        }
        else if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            if (EquippedItem == null) return;
            EquippedItem.isUsingItem = false;
            if (EquippedItem.ItemName == "Extinguisher" && EquippedItem.CanAttack)
            {
                trigger.allowParticle = false;
                pSound.ExtinguisherStop();
            }
        }
        else if (Input.GetKeyDown(KeyCode.F))
        {
            PickUpItem();
            if (CollidedItems[0] == null)
            {
                // DROP CURRENTLY EQUIPPED ITEM
                // NO ITEMS NEARBY FREE TO DROP ITEM
                if (EquippedItem == null || EquippedItem.ItemName == "Axe" || !this.gameObject.GetComponent<PlayerController>().grounded) return;
                EquippedItem.DropItem();
                EquipmentList[EquipmentList.FindIndex(ind => ind.Equals(EquippedItem))] = EmptyItem;
                EquippedItem = EmptyItem;
            }
        }
    }

    public void PickUpItem()
    {
        CollidedItems = new Collider2D[1];
        this.GetComponent<Collider2D>().OverlapCollider(new ContactFilter2D()
        {
            layerMask = LayerMask.GetMask("Weapon"),
            useTriggers = true,
            useLayerMask = true
        }, CollidedItems);

        if (CollidedItems[0] == null)
        {
            print("No Items Free to drop");
            return;
        }

        if (CollidedItems[0].GetComponent<Item>() && !CollidedItems[0].GetComponent<Towel>())
        {
            CollidedItems[0].gameObject.transform.position = AttackTransform.position;
            CollidedItems[0].GetComponent<Item>().CarryItem(this.gameObject);
            EquippedItem = CollidedItems[0].GetComponent<Item>();
            if (CollidedItems[0].GetComponent<Extinguisher>())
            {
                if (EquipmentList[1].ItemName == "Extinguisher")
                {
                    EquipmentList.Find(Item => Item.ItemName == "Extinguisher").DropItem();
                    EquipmentList[1] = CollidedItems[0].GetComponent<Extinguisher>();
                }
                else
                {
                    EquipmentList[1] = CollidedItems[0].GetComponent<Extinguisher>();
                }
            }
            else if (CollidedItems[0].GetComponent<Axe>())
            {
                if (EquipmentList[1].ItemName == "Axe")
                {
                    EquipmentList.Find(Item => Item.ItemName == "Axe").DropItem();
                    EquipmentList[0] = CollidedItems[0].GetComponent<Axe>();
                }
                else
                {
                    EquipmentList[0] = CollidedItems[0].GetComponent<Axe>();
                }
            }
            else if (CollidedItems[0].GetComponent<WaterBottle>())
            {
                if (EquipmentList[1].ItemName == "Bottle")
                {
                    EquipmentList.Find(Item => Item.ItemName == "Bottle").DropItem();
                    EquipmentList[2] = CollidedItems[0].GetComponent<WaterBottle>();
                }
                else
                {
                    EquipmentList[2] = CollidedItems[0].GetComponent<WaterBottle>();
                }
            }
            EquipmentList.Add(EquippedItem);
            Debug.Log("Weapon PickUp");
        }
        else if (CollidedItems[0].GetComponent<Towel>() && UsableList.Count <= 4)
        {
            CollidedItems[0].gameObject.transform.position = AttackTransform.position;
            CollidedItems[0].GetComponent<Item>().CarryItem(this.gameObject);
            UsableList.Add(CollidedItems[0].GetComponent<Towel>());
        }
        else
        {
            print("Full Inventory");
        }
    }
    void OnDrawGizmosSelected()
    {
        // Draws a 5 unit long red line in front of the object
        Gizmos.color = Color.red;
        Vector3 direction = (AttackTransform.position - this.gameObject.transform.position);
        Gizmos.DrawRay(this.gameObject.transform.position, direction);
    }
}
