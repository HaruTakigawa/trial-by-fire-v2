﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderConrtroller : MonoBehaviour
{
    public BoxCollider2D standing;
    public BoxCollider2D crouching;

    PlayerController player;
    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<PlayerController>();
        standing.enabled = true;
        crouching.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!player.grounded)
        {
            standing.enabled = true;
            crouching.enabled = false;
        }
        else
        {
            if (player.isCrouching)
            {
                standing.enabled = false;
                crouching.enabled = true;
            }
            else
            {
                standing.enabled = true;
                crouching.enabled = false; 
            }
        }
    }
}
