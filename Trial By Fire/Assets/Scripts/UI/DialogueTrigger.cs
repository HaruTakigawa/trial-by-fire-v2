﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueTrigger : MonoBehaviour
{
    // Goes to the different places that initiates a thought on the character
    public TextMeshProUGUI Sentence;
    public string[] DialogueText;
    private int index;

    public float typeSpeed;

    //public GameObject contButton;
    //public GameObject controls;
    //public GameObject play;
    public PlayerController controller;
    public Inventory inventory;
    public GameObject continuePrompt;
    public GameObject skipPrompt;
    public GameObject background;
    public UIController uiController;
    //public Image PlayerPortrait;
    //private Image portrait;

    private void Start()
    {
        ResetTextDisplay();
        //contButton.SetActive(false);
        //play.SetActive(false);
        continuePrompt.SetActive(false);
        skipPrompt.SetActive(false);
    }

    //StopAllCoroutines();
    //ResetTextDisplay();
    //Sentence.text = DialogueText[index];

    private void Update()
    {
        if (Sentence.text.Length != DialogueText[index].Length)
        {
            // Skips dialogue
            if (Input.GetKey(KeyCode.Mouse1))
            {
                StopAllCoroutines();
                ResetTextDisplay();
                Time.timeScale = 1.0f;
                controller.enabled = true;
                inventory.enabled = true;
                continuePrompt.SetActive(false);
                skipPrompt.SetActive(false);
                background.SetActive(false);
            }
            //// Completely types sentence
            //if (Input.GetKey(KeyCode.Mouse0) && !uiController.isPaused)
            //{
            //    Sentence.text = "";
            //    StopAllCoroutines();
            //    Sentence.text = DialogueText[index];
            //    Time.timeScale = 1.0f;
            //    controller.enabled = true;
            //    inventory.enabled = true;
            //    continuePrompt.SetActive(false);
            //    skipPrompt.SetActive(false);
            //}
        }

        if (Sentence.text.Length == DialogueText[index].Length)
        {
            continuePrompt.SetActive(true);
            skipPrompt.SetActive(false);
            if (Input.GetKey(KeyCode.Mouse0) && !uiController.isPaused)
            {
                NextSentence();
                continuePrompt.SetActive(false);
                background.SetActive(false);
            }
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "Player")
        {
            Time.timeScale = 0.0f;
            index = 0; //sets the index to 0 to begin at the first sentence
            StartCoroutine(Type());
            controller.enabled = false;
            controller.isMoving = false;
            inventory.enabled = false;
            skipPrompt.SetActive(true);
            background.SetActive(true);
        }

        if (Sentence.text == null) return;
        Sentence.enabled = true;
    }

    void OnTriggerExit2D(Collider2D col)
    {
        //portrait.enabled = false;
        Sentence.enabled = false;
        index = 0;
        ResetTextDisplay();
        continuePrompt.SetActive(false);
        background.SetActive(false);

        if (col.gameObject.tag == "Player")
        {
           Destroy(gameObject);
        }
    }

    IEnumerator Type() //This script gives the sentence typing effect
    {
        foreach (char letter in DialogueText[index].ToCharArray())
        {
            Sentence.text += letter;
            yield return new WaitForSeconds(typeSpeed);
        }
        if (index >= DialogueText.Length - 1)
        {
            //ShowControls();
            StopCoroutine(Type());
        }
    }

    public void NextSentence() //Moves on to the next sentence in the PlayerDialogue array
    {
        //contButton.SetActive(true);

        if (index < DialogueText.Length - 1)
        {
            index++;
            ResetTextDisplay();

            StartCoroutine(Type());
        }
        else
        {
            ResetTextDisplay();
            Time.timeScale = 1.0f;
            controller.enabled = true;
            inventory.enabled = true;
            //contButton.SetActive(false);
            //this.enabled = false;
        }
    }

    private void ResetTextDisplay()
    {
        Sentence.text = "";
    }

    //private void ShowControls() //Don't know if we need this, might remove it if it ends up unused
    //{
    //    //contButton.SetActive(false);
    //    //controls.SetActive(true);
    //    //play.SetActive(true);
    //}
}
