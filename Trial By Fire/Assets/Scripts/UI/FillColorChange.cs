﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FillColorChange : MonoBehaviour 
{
    private LungBar fill;
    public Image fillImage;
    private Color defaultColor;
    public Color Color1;
    public Color Color2;

    // Start is called before the first frame update
    void Start()
    {
        fill = gameObject.GetComponent<LungBar>();
        defaultColor = fillImage.color;
    }

    void Update()
    {
        if(fill.CurrentCapacity < 49)
        {
            fillImage.color = defaultColor;
        }
        if(fill.CurrentCapacity > 50)
        {
            fillImage.color = Color1;
        }
        if(fill.CurrentCapacity > 99)
        {
            fillImage.color = Color2;
        }
    }
}
