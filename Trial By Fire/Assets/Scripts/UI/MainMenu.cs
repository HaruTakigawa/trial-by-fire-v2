﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MainMenu : MonoBehaviour
{
    public UIController UI;
    // Start is called before the first frame update
    void Start()
    {
        //Destroy(DGameManager.Instance.gameObject);
        //Destroy(TempSaveFire.instance.gameObject);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void StartBtn()
    {
        SceneManager.LoadScene("TutorialScene");
    }

    public void ExitBtn()
    {
        Application.Quit();
    }

    public void MainMenuBtn()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void PauseBtn()
    {
        UI.controller.inventory.trigger.allowParticle = false;
        UI.PauseMenu.SetActive(true);
        UI.isPaused = true;
        UI.PauseButton.SetActive(false);
        UI.controller.enabled = false;
        UI.inventory.enabled = false;
        Time.timeScale = 0.0f;
    }

    public void ResumeBtn()
    {
        UI.PauseMenu.SetActive(false);
        UI.PauseButton.SetActive(true);
        UI.isPaused = false;
        Time.timeScale = 1.0F;
        UI.controller.enabled = true;
        UI.inventory.enabled = true;
    }
}
