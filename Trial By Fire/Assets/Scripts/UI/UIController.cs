﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public GameObject EquippedItemExtUI;
    public GameObject EquippedItemAxeUI;
    public GameObject EquippedItemTowelUI;
    public GameObject EquippedItemTowel2UI;
    public GameObject EquippedItemTowel3UI;
    public GameObject EquippedItemTowel4UI;
    public GameObject ExtSelectedUI;
    public GameObject AxeSelectedUI;
    public GameObject TowelSelectedUI;
    public GameObject PauseMenu;
    public GameObject PauseButton;
    public bool isPaused;
    //public GameObject ExtinguisherUI;
    public PlayerController controller;
    public Inventory inventory;
    // Start is called before the first frame update
    void Start()
    {
        EquippedItemExtUI.SetActive(false);
        EquippedItemAxeUI.SetActive(false);
        EquippedItemTowelUI.SetActive(false);
        EquippedItemTowel2UI.SetActive(false);
        EquippedItemTowel3UI.SetActive(false);
        EquippedItemTowel4UI.SetActive(false);
        ExtSelectedUI.SetActive(false);
        AxeSelectedUI.SetActive(false);
        TowelSelectedUI.SetActive(false);
        PauseMenu.SetActive(false);
        PauseButton.SetActive(true);

        //ExtinguisherUI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (controller.inventory.EquippedItem != null)
        {
            EquippedItemExtUI.SetActive(false);
            EquippedItemAxeUI.SetActive(false);
            ExtSelectedUI.SetActive(false);
            AxeSelectedUI.SetActive(false);
            //ExtinguisherUI.SetActive(false);
        }
        if (controller.inventory.EquipmentList.Exists(Item => Item.ItemName == "Extinguisher"))
        {
            // Set the Extinguisher UI to True;
            EquippedItemExtUI.SetActive(true);
        }


        if (controller.inventory.EquipmentList.Exists(Item => Item.ItemName == "Axe"))
        {
            EquippedItemAxeUI.SetActive(true);
        }


        if (controller.inventory.UsableList.Exists(Item => Item.ItemName == "Towel1"))
        {
            EquippedItemTowelUI.SetActive(true);
            //Debug.Log("Towel1");
        }

        if (controller.inventory.UsableList.Exists(Item => Item.ItemName == "Towel2"))
        {
            EquippedItemTowel2UI.SetActive(true);
        }

        if (controller.inventory.UsableList.Exists(Item => Item.ItemName == "Towel3"))
        {
            EquippedItemTowel3UI.SetActive(true);
        }

        if (controller.inventory.UsableList.Exists(Item => Item.ItemName == "Towel4"))
        {
            EquippedItemTowel4UI.SetActive(true);
        }

        if (controller.inventory.EquippedItem.ItemName == "Extinguisher")
        {
            ExtSelectedUI.SetActive(true);
            AxeSelectedUI.SetActive(false);
            TowelSelectedUI.SetActive(false);
        }

        if (controller.inventory.EquippedItem.ItemName == "Axe")
        {
            AxeSelectedUI.SetActive(true);
            ExtSelectedUI.SetActive(false);
            TowelSelectedUI.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            controller.inventory.trigger.allowParticle = false;
            Time.timeScale = 0.0f;
            PauseMenu.SetActive(true);
            isPaused = true;
            PauseButton.SetActive(false);
            controller.enabled = false;
            inventory.enabled = false;
        }

        //if (controller.inventory.EquippedItem.ItemName == "Towel")
        //{
        //    TowelSelectedUI.SetActive(true);
        //    AxeSelectedUI.SetActive(false);
        //    ExtSelectedUI.SetActive(false);
        //}
        // else
        // {
        //     EquippedItemExtUI.SetActive(false);
        //     EquippedItemAxeUI.SetActive(false);
        //     print("Doesn't Exist");
        // }
    }
    // void LateUpdate()
    // {
    //     if (controller.inventory.EquippedItem == null)
    //     {
    //         EquippedItemExtUI.SetActive(false);
    //         EquippedItemAxeUI.SetActive(false);
    //         //ExtinguisherUI.SetActive(false);
    //     }

    //     else if (controller.inventory.EquipmentList[0] && controller.inventory.EquippedItem.ItemName == "Extinguisher")
    //     {
    //         Debug.Log("ExtUI Active");
    //         EquippedItemExtUI.SetActive(true);
    //         // ExtinguisherUI.SetActive(true);
    //         EquippedItemAxeUI.SetActive(false);
    //     }

    //     else if (controller.inventory.EquipmentList[1] && controller.inventory.EquippedItem.ItemName == "Axe")
    //     {
    //         Debug.Log("AxeUI Active");
    //         EquippedItemAxeUI.SetActive(true);
    //         //ExtinguisherUI.SetActive(false);
    //     }

    //     else
    //     {

    //     };

    //}
}
