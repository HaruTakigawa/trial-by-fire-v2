﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class TimerBar : MonoBehaviour
{
    public float CurrentTimer;
    public float MaxTimer;
    public float t;
    public Image BarImage;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CurrentTimer += Time.deltaTime;
        t = CurrentTimer / MaxTimer;

        BarImage.fillAmount = t;

        if (CurrentTimer >= MaxTimer)
        {
            SceneManager.LoadScene("LoseScene");
        }
    }
}
