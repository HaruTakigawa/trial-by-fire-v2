﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
public class TimerText : MonoBehaviour
{
    public Text TimeText;
    //public TextMeshProUGUI TimeTextShadow;
    private float timeNo;
    static public int minutes;
    static public int seconds;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timeNo += Time.deltaTime;

        seconds = (int)(timeNo % 60);
        minutes = (int)(timeNo / 60);

        string timerString = string.Format("{0:00}:{1:00}", minutes, seconds);

        TimeText.text = timerString;
        //TimeTextShadow.text = timerString;
    }
}
