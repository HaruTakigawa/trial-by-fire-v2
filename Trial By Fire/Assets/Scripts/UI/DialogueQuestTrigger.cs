﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueQuestTrigger : MonoBehaviour
{
    // Goes to the different places that initiates a thought on the character
    public TextMeshProUGUI Sentence;
    public string[] DialogueText;
    private int index;

    public float typeSpeed;

    public PlayerController controller;
    public Inventory inventory;
    public GameObject continuePrompt;
    public GameObject skipPrompt;
    public GameObject background;
    public UIController uiController;
    public GameObject trigger;
    //public GameObject BathroomWinTrigger;
    //public GameObject QuestTrigger;

    private void Start()
    {
        //Sentence.enabled = false;
        ResetTextDisplay();
        continuePrompt.SetActive(false);
        skipPrompt.SetActive(false);
        ResetTextDisplay();
        //BathroomWinTrigger.SetActive(false);

    }

    //StopAllCoroutines();
    //ResetTextDisplay();
    //Sentence.text = DialogueText[index];

    private void Update()
    {
        if (Sentence.text.Length != DialogueText[index].Length)
        {
            // Skips dialogue
            if (Input.GetKey(KeyCode.Mouse0) || Input.GetKey(KeyCode.Mouse1))
            {
                trigger.SetActive(false);
                ResetTextDisplay();
                Time.timeScale = 1.0f;
                controller.enabled = true;
                inventory.enabled = true;
                continuePrompt.SetActive(false);
                skipPrompt.SetActive(false);
                background.SetActive(false);
            }
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "Player")
        {
            Time.timeScale = 0.0f;
            index = 0; //sets the index to 0 to begin at the first sentence
            //StartCoroutine(Type());
            controller.enabled = false;
            controller.isMoving = false;
            inventory.enabled = false;
            continuePrompt.SetActive(true);
            background.SetActive(true);
            CheckForTowels();
            Sentence.enabled = true;
        }

        if (Sentence.text == null) return;
        Sentence.enabled = true;
    }

    void OnTriggerExit2D(Collider2D col)
    {
        StartCoroutine(CollisionTimer());
        Sentence.enabled = false;
        index = 0;
        ResetTextDisplay();
        continuePrompt.SetActive(false);
        background.SetActive(false);
        Sentence.enabled = false;
        trigger.SetActive(false);

        if (controller.inventory.UsableList.Count == 4)
        {
            trigger.SetActive(false);
        }
    }

    IEnumerator CollisionTimer()
    {
        this.enabled = false;
        yield return new WaitForSeconds(5.0f);
        this.enabled = true;
    }

    //IEnumerator Type() //This script gives the sentence typing effect
    //{
    //    foreach (char letter in DialogueText[index].ToCharArray())
    //    {
    //        Sentence.text += letter;
    //        yield return new WaitForSeconds(typeSpeed);
    //    }
    //    if (index >= DialogueText.Length - 1)
    //    {
    //        //ShowControls();
    //        StopCoroutine(Type());
    //    }
    //}

    //public void NextSentence() //Moves on to the next sentence in the PlayerDialogue array
    //{
    //    //contButton.SetActive(true);

    //    if (index < DialogueText.Length - 1)
    //    {
    //        index++;
    //        ResetTextDisplay();

    //        StartCoroutine(Type());
    //    }
    //    else
    //    {
    //        ResetTextDisplay();
    //        Time.timeScale = 1.0f;
    //        controller.enabled = true;
    //        inventory.enabled = true;
    //    }
    //}

    private void ResetTextDisplay()
    {
        Sentence.text = "";
    }

    void CheckForTowels()
    {
        if (controller.inventory.UsableList.Count == 0)
        {
            Sentence.text = "Looks like I can seal the door with towels in here.. I'll probably need around four.";

        }

        if (controller.inventory.UsableList.Count == 1)
        {
            Sentence.text = "Now this should be useful.";
        }

        if (controller.inventory.UsableList.Exists(Item => Item.ItemName == "Towel1") & controller.inventory.UsableList.Exists(Item => Item.ItemName == "Towel2") & controller.inventory.UsableList.Exists(Item => Item.ItemName == "Towel3") & controller.inventory.UsableList.Exists(Item => Item.ItemName == "Towel4"))
        {
            Sentence.text = "Alright this should be enough, now I just need to find the right room.";
        }
    }

}
