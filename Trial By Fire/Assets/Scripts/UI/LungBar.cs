﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LungBar : MonoBehaviour
{
    public float CurrentCapacity;
    private float MaxCapacity;
    public Image BarImage;

    public Smoke Smokes;

    // Update is called once per frame
    void Update()
    {
        CurrentCapacity = Smokes.curSmoke;
        MaxCapacity = Smokes.maxSmoke;


        BarImage.fillAmount = CurrentCapacity / MaxCapacity;
    }
}
