﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class TextTransfer : MonoBehaviour
{
    private float timeNo;
    static public int seconds;
    static public int minutes;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        timeNo += Time.deltaTime;

        seconds = (int)(timeNo % 60);
        minutes = (int)(timeNo / 60);

        string timerString = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "Player")
        {
            StartCoroutine(NextScene());
        }
    }

    IEnumerator NextScene()
    {
        yield return SceneManager.LoadSceneAsync("WinScene", LoadSceneMode.Additive);
        yield return null;

        // Set the timer value here
        GameObject timerText = GameObject.Find("TimerText");

        timerText.GetComponent<Text>().text = string.Format("Escaped in: {0:00}:{1:00}", minutes, seconds);

        SceneManager.UnloadSceneAsync(gameObject.scene);
    }
}
