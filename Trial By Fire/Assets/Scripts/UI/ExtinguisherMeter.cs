﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExtinguisherMeter : MonoBehaviour
{
    private float MaxMeter;
    private float CurMeter;
    public Image BarImage;
    public PlayerController Player;
    Extinguisher ext;
    void Start()
    {
        ext = GetComponent<Extinguisher>();
        // Player = GetComponent<PlayerController>();
    }

    void Update()
    {
        if (Player.inventory.EquippedItem == null) return;
        if (Player.inventory.EquippedItem.ItemName == "Extinguisher")
        {
            // needs fix cant find equipped item  need list.find
            CurMeter = Player.inventory.EquippedItem.GetComponent<Extinguisher>().curUses;
            MaxMeter = Player.inventory.EquippedItem.GetComponent<Extinguisher>().maxUses;
            BarImage.enabled = true;
            //Debug.Log("Bar");
        }

        if (Player.inventory.EquippedItem.ItemName != "Extinguisher")
        {
            BarImage.enabled = false;
        }
            //else
            //{
            //    Debug.Log("No Extinguisher");
            //}
            BarImage.fillAmount = (MaxMeter-CurMeter) / MaxMeter;
    }
}
