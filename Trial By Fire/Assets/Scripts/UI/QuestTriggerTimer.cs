﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class QuestTriggerTimer : MonoBehaviour
{
    public DialogueQuestTrigger Quest;
    public GameObject Trigger;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Trigger.activeInHierarchy == false)
        {
            Debug.Log("Trigger is inactive");
            StartCoroutine(CollisionTimer());
        }
    }

    public IEnumerator CollisionTimer()
    {
        yield return new WaitForSeconds(5.0f);
        Trigger.SetActive(true);
    }
}
