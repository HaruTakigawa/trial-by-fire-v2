﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TimerMeter : MonoBehaviour
{
    public float CurTimer;
    public float MaxTimer;
    public float t;
    // public RectTransform FiretruckIcon;
    // public RectTransform StartPoint;
    // public RectTransform TargetPoint;
    public Transform FiretruckIcon;
    public Transform StartPoint;
    public Transform TargetPoint;
    void Awake()
    {
        StartPoint.position = FiretruckIcon.position;
    }
    void Update()
    {
        CurTimer += Time.deltaTime;
        t = CurTimer/MaxTimer;
        
        //((RectTransform)transform).anchoredPosition = Vector2.MoveTowards(StartPoint.anchoredPosition, TargetPoint.anchoredPosition,t);
        transform.position = Vector2.Lerp(StartPoint.position,TargetPoint.position,t);
        if (CurTimer >= MaxTimer)
        {
            SceneManager.LoadScene("LoseScene");
        }
    }
}
