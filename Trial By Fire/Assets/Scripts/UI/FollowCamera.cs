﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    public float SmoothSpeed;
    public GameObject Target;
    public GameObject Normal;
    public GameObject Slow;
    public GameObject Weak;
    public Vector3 Offset;
    void FixedUpdate()
    {
        if (Normal.activeSelf)
        {
            Target = Normal;
        }
        else if (Slow.activeSelf)
        {
            Target = Slow;
        }
        else if (Weak.activeSelf)
        {
            Target = Weak;
        }
        Vector3 DesiredPosition = Target.transform.position + Offset;
        Vector3 SmoothedPosition = Vector3.Lerp(transform.position, DesiredPosition, SmoothSpeed);
        transform.position = SmoothedPosition;
    }
}
