﻿Shader "Unlit/Outline"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Color("Color", color) = (1,1,1,1)
	}
		SubShader
		{
			Cull off
			Blend One OneMinusSrcAlpha
			Tags { "RenderType" = "Opaque" }
			LOD 100

			Pass
			{

				CGPROGRAM

				#pragma vertex vertexFunc
				#pragma fragment fragmentFunc
				#include "UnityCG.cginc"

				sampler2D _MainTex;

			struct v2f
			{
				float4 pos : SV_POSITION;
				half2 uv : TEXCOORD0;
			};

			v2f vertexFunc(appdata_base v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.texcoord;

				return o;
			}

			fixed4 _Color;
			float4 _MainTex_TexelSize;

			fixed4 fragmentFunc(v2f i) : COLOR
			{
				half4 c = tex2D(_MainTex, i.uv);
				half4 outlineC = _Color;

				c.rgb *= c.a;
				outlineC.a *= ceil(c.a);

				fixed upAlpha = tex2D(_MainTex, i.uv + fixed2(0, _MainTex, _MainTex_TexelSize.y)).a;
				fixed downAlpha = tex2D(_MainTex, i.uv + fixed2(0, _MainTex, _MainTex_TexelSize.y)).a;
				fixed rightAlpha = tex2D(_MainTex, i.uv + fixed2(0, _MainTex, _MainTex_TexelSize.x, 0)).a;
				fixed leftAlpha = tex2D(_MainTex, i.uv + fixed2(0, _MainTex, _MainTex_TexelSize.x, 0)).a;

				return lerp(outlineC, c, ceil(upAlpha * downAlpha * leftAlpha * rightAlpha));
			}
	
			ENDCG

        }
    }
}
