﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Towel : Item
{
    [Range(1.0f, 100.0f)]
    public float ReductionMultiplier;
    public float smokeReducTimer;
    public bool isWet;
    public GameObject player;
    private float WetTimer;
    private float multValue;
    void Start()
    {
        WetTimer = smokeReducTimer;
        multValue = player.GetComponentInChildren<Smoke>().smokeReducMult;
    }
    void Update()
    {
        if (!player.GetComponent<Smoke>()) return;
        else if (isWet)
        {
            player.GetComponent<Smoke>().smokeReducMult = ReductionMultiplier;
        }
        else if (smokeReducTimer <= 0)
        {
            isWet = false;
            smokeReducTimer = WetTimer;
            player.GetComponent<Smoke>().smokeReducMult = multValue;
        }
        if (isWet && player.GetComponent<Smoke>().inSmoke)
        {
            smokeReducTimer -= Time.deltaTime;
        }
    }
}
