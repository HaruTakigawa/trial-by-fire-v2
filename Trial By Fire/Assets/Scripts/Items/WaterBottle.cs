﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterBottle : Item
{
    public float smokeReduction;

    public GameObject player;
    
    void Start()
    {
        //player = GetComponent<PlayerController>();
    }
    public override void UseItem()
    {
        if (CanAttack)
        {
           
            isUsingItem = true;
            print("Water Bottle Dranked");
            player.GetComponent<PlayerController>().inventory.EquipmentList.Remove(player.GetComponent<PlayerController>().inventory.EquippedItem);
            player.GetComponent<PlayerController>().inventory.EquippedItem = null;
            player.GetComponent<Smoke>().curSmoke -= smokeReduction;
           
        }

    }
}
