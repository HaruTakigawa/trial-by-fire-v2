﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Axe : Item
{
    public override void UseItem()
    {
        if (CanAttack)
        {
            isUsingItem = true;
            print("Axe attacked");
            base.UseItem();
            Collider2D[] ObjectsToDamage = Physics2D.OverlapCircleAll(inventory.AxeAttackTransform.position, AttackRange, Layer);
            for (int i = 0; i < ObjectsToDamage.Length; i++)
            {
                ObjectsToDamage[i].GetComponent<Barricade>().TakeDamage(Damage);
            }
        }
    }
     void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(inventory.AxeAttackTransform.position, AttackRange);
    }
}
