﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Extinguisher : Item
{
    public float curUses;
    public float maxUses;
    public bool usable;
    public Collider2D[] ObjectsToDamage;
    public override void UseItem()
    {
        if (CanAttack && curUses < maxUses)
        {
            isUsingItem = true;
            curUses += Time.deltaTime * 1;
            base.UseItem();
            ObjectsToDamage = Physics2D.OverlapCircleAll(inventory.AttackTransform.position, AttackRange, Layer);
            for (int i = 0; i < ObjectsToDamage.Length; i++)
            {
                if (ObjectsToDamage[i].GetComponent<Fires>())
                {
                    ObjectsToDamage[i].GetComponent<Fires>().TakeDamage(Damage);
                }

            }
        }
    }

    void Update()
    {
        if (curUses >= maxUses)
        {
            usable = false;
            CanAttack = false;
        }
        else if (curUses < maxUses)
        {
            usable = true;
            CanAttack = true;
        }
    }
    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(inventory.AttackTransform.position, AttackRange);
    }
}
