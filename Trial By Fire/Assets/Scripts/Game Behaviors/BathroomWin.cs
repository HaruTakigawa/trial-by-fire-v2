﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class BathroomWin : MonoBehaviour
{
    //Interact both doors with towels

    public Bathroom bathroomDoor;
    public GameObject BathroomWinArea;
    // public LockerRoom lockerDoor;
    PlayerController player;
    public GameObject Survivor;
    void Start()
    {
        player = GetComponent<PlayerController>();
    }
    void Update()
    {
        if (bathroomDoor.bathroomDoorLock)
        {
            BathroomWinArea.transform.position = Survivor.transform.position;
            print("Bathroom WIN");
        }
    }
}
