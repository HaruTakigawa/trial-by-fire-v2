﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deactivateCollider : MonoBehaviour
{
    BoxCollider2D coll;

    void Start()
    {
        coll = GetComponent<BoxCollider2D>();
    }

    public void colliderOff()
    {
        coll.enabled = false;
    }
}
