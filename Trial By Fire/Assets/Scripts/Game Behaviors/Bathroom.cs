﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bathroom : MonoBehaviour
{
    public bool bathroomDoorLock;
    void Update()
    {
        if (bathroomDoorLock)
        {
            this.GetComponent<BoxCollider2D>().isTrigger = false;
        }
    }
}
