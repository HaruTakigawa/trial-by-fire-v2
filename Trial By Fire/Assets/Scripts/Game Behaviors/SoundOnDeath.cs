﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundOnDeath : MonoBehaviour
{
    Barricade b;
    AudioManager sound;

    bool canPlay;

    // Start is called before the first frame update
    void Start()
    {
        b = this.gameObject.GetComponent<Barricade>();
        sound = FindObjectOfType<AudioManager>();
        canPlay = true;
    }

    void Update()
    {
        if(canPlay)
        {
            if (b.maxHealth <= 1)
            {
                if (b.curHealth <= 0)
                {
                    sound.Play("GlassShatter");
                    canPlay = false;
                }
            }
            if (b.maxHealth > 1)
            {
                if (b.curHealth <= 0)
                {
                    sound.Play("WoodDestroy");
                    sound.Play("WoodCrack");
                    canPlay = false;
                }
            }
        }
    }
}
