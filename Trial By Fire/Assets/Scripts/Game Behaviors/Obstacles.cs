﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacles : MonoBehaviour
{
    public float maxHealth;
    public float curHealth;
    public float percentageHealth;
    public bool isRespawnable;
    public GameObject dyingParticleEffect;

    void Update()
    {
        SetPercentageHealth();
        if (curHealth <= 0) this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
    }

    public void SetPercentageHealth()
    {
        percentageHealth = curHealth / maxHealth;
    }
    public virtual void TakeDamage(float Damage)
    {
    
        curHealth -= Damage;
        //Instantiate(dyingParticleEffect, this.transform.position,transform.rotation);
    }
}
