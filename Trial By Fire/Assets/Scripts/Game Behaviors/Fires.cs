﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fires : Obstacles
{
    public GameObject spreadFire;
    public float SpawnArea;
    public float spreadThreshold;
    public float maxSpreadThreshold;
    public float thresholdSpeed;
    public float timeToRespawn;
    public bool spawned;
    public bool destroyed;
    public GameObject lighting;
    public GameObject fireParticles;
    public GameObject smokeParticles;
    private float respawnTimer;

    void Start()
    {
        respawnTimer = timeToRespawn;
    }

    // Update is called once per frame
    void Update()
    {
        if (timeToRespawn <= 0) RespawnFire();

        this.gameObject.SetActive(true);
        if (spreadThreshold >= maxSpreadThreshold && !spawned)
        {
            if (spreadThreshold >= maxSpreadThreshold) spreadThreshold = maxSpreadThreshold;
            spawned = true;
            spreadThreshold = 0;
            SpawnFire();
        }
        else
        {
            if (destroyed) return;
            spreadThreshold = spreadThreshold + (thresholdSpeed * Time.deltaTime);
        }
    }
    void RespawnFire()
    {
        this.gameObject.SetActive(true);
        destroyed = false;
        lighting.SetActive(true);
        fireParticles.SetActive(true);
        smokeParticles.SetActive(true);
        this.gameObject.GetComponent<BoxCollider2D>().enabled = true;
        this.gameObject.GetComponent<SpriteRenderer>().enabled = true;
        timeToRespawn = respawnTimer;
    }
    void SpawnFire()
    {
        Vector3 RandomPosition = new Vector3(Random.Range(-SpawnArea / 2, SpawnArea / 2), spreadFire.transform.position.y, 0);
        Vector3 SpawnPoint = RandomPosition;
        GameObject SpawnedFire = (GameObject)Instantiate(spreadFire, SpawnPoint, Quaternion.identity);
        Fires fire = SpawnedFire.GetComponent<Fires>();
        curHealth = 20;
        fire.spreadThreshold = 1;
        fire.spawned = false;
        fire.transform.position = SpawnPoint;
    }
    public override void TakeDamage(float Damage)
    {
        //play audio for fire extinguisher
        if (curHealth <= 0)
        {
            if (isRespawnable)
            {
                destroyed = true;
                lighting.gameObject.SetActive(false);
                fireParticles.gameObject.SetActive(false);
                smokeParticles.gameObject.SetActive(false);
                this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
                this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            }
            else
                Destroy(this.gameObject);
        }
        else
        {
            base.TakeDamage(Damage);
        }

    }
}
