﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceSmoke : MonoBehaviour
{
    RaycastHit2D collisionPoint;
    public GameObject startPoint;
    public Vector2 offset;

    Vector3 direction;

    // Update is called once per frame

    private void Start()
    {
        offset = new Vector2(0, -.5f);
    }

    void Update()
    {
        collisionPoint = Physics2D.Raycast(startPoint.gameObject.transform.position, new Vector2(0, 1));
        if (collisionPoint.collider != null)
        {
            this.transform.position = new Vector2(collisionPoint.point.x + offset.x, collisionPoint.point.y + offset.y);
        }
    }
}

