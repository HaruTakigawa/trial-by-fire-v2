﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockerRoom : MonoBehaviour
{
    public bool lockerDoorLock;
    void Update()
    {
        if (lockerDoorLock)
        {
            this.GetComponent<BoxCollider2D>().isTrigger = false;
        }
    }
}
