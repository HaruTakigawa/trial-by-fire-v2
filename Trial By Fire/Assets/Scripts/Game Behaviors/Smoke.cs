﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Smoke : MonoBehaviour
{
    public float curSmoke;
    public float maxSmoke;
    public float resettedSmoke;
    public float resetSmokeIncrease;
    [Range(0.1f,1f)]
    public float smokeReducMult = 1;
    public float coughDuration;
    public bool isCoughing;
    private float dur;
    private float playerSpeed;
    public bool inSmoke;
    PlayerController player;
    void Start()
    {
        player = this.GetComponentInParent<PlayerController>();
        playerSpeed = player.speed;
        dur = coughDuration;
    }
    void Update()
    {
        if (curSmoke <= 0) curSmoke = 0;
        if (curSmoke >= maxSmoke)
        {
            if (coughDuration > 0)
            {
                coughDuration -= Time.deltaTime;
                isCoughing = true;
                player.speed = 0;
                return;
            }
            else
            {
                isCoughing = false;
                player.speed = playerSpeed;
                coughDuration = dur;
                curSmoke = resettedSmoke;
                if (resettedSmoke > maxSmoke)
                {
                    resettedSmoke = maxSmoke;
                    print("Kill Player Here");
                }
                else
                {
                    resettedSmoke += resetSmokeIncrease;
                }
            }
        }
        if (!inSmoke)
        {
            curSmoke -= Time.deltaTime;
        }
    }
    void FixedUpdate()
    {
        if (inSmoke)
        {
            curSmoke += Time.deltaTime / smokeReducMult;

        }
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Smoke") inSmoke = true;
    }
    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "Smoke")
        {
            if (col.GetComponentInParent<Fires>().curHealth < 0)
            {
                inSmoke = false;
            }
        }

    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "Smoke")
        {
            inSmoke = false;
        }
    }
}
