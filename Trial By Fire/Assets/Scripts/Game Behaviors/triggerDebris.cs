﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triggerDebris : MonoBehaviour
{ public ParticleSystem[] debris;
    public GameObject player;
    PlayerController pController;
    void Start()
    {
        debris = GetComponentsInChildren<ParticleSystem>();
        pController = player.gameObject.GetComponent<PlayerController>();
    }
    public void startparticles()
    {
        int i = 0;
        while (i != debris.Length)
        {
            if (pController.m_FacingRight)
            {
                debris[i].transform.localScale = new Vector3(1, 1, 1);
            }
            else
            {
                debris[i].transform.localScale = new Vector3(1, 1, -1);
            }
            Instantiate(debris[i], this.transform.position, Quaternion.Euler(-180, -90, 90), this.transform);
            i += 1;
        }
    }
}
