﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barricade : Obstacles
{

    triggerDebris trigger;
    public bool isPlayable;
    public float triggerParticleInterval;
    void Start()
    {
        trigger = this.GetComponent<triggerDebris>();
        isRespawnable = false;
    }
    public override void TakeDamage(float Damage)
    {
        if (curHealth <= 0)
        {
            this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }
        else
        {
            base.TakeDamage(Damage);
        }
        if (trigger == null) return;
        StartCoroutine("PlayParticle");
        //play audio barricade
    }

    IEnumerator PlayParticle()
    {
        if (isPlayable)
        {
            trigger.startparticles();
            isPlayable = false;
            yield return new WaitForSeconds(triggerParticleInterval);
            isPlayable = true;
        }
    }
}
