﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerParticles : MonoBehaviour
{
    ParticleSystem part;
    public bool emitParticle = false;

    private void Start()
    {
        part = GetComponent<ParticleSystem>();
    }

    private void Update()
    {
        if(emitParticle)
        {
            Invoke("playParticle", 0);
        }
        else
        {
            StopParticle();
            CancelInvoke();
        }
    }


    private void PlayParticle()
    {
        part.Play();
    }

    private void StopParticle()
    {
        part.Stop();
    }

}
